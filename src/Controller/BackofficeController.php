<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Images;
use App\Form\ArticleType;
use App\Form\CategPrdType;
use App\Form\ImagesType;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\DevisRepository;
use App\Repository\FactureRepository;
use App\Repository\ImagesRepository;
use App\Repository\ProduitRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Produit;
use App\Form\ProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;

class BackofficeController extends AbstractController
{
	/**
	 *  Index du backoffice permet l'affichage des informations principales (Nouvel achat client, factures, devis client)
	 * TODO faire la partie d'affichage des informations principales
	 * @Route("/admin", name="backoffice")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 * @param FactureRepository $facture
	 * @return Response
	 */
    public function index(FactureRepository $facture): Response
    {
		$achat = $facture->findAll();
		$client=[];
	    foreach ($achat as $ach) {
		    $client[]=$ach->getListeProds();
		}

        return $this->render('backoffice/index.html.twig', [
            'controller_name' => 'BackofficeController',
	        'client'=>$client,
	        'achat'=>$achat
        ]);
    }


	/**
	 *  Création de produit depuis un formulaire lié au backoffice, permet également la création de catégorie de produit
	 * (Un produit ne peut être créé sans catégorie. )
	 *
	 * @Route("/admin-gestionPrd", name="backoffice_prd")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 * @param ManagerRegistry $doctrine
	 * @param Request $request
	 * @param CategoryRepository $lesCateg
	 * @param ProduitRepository $lesProds
	 * @param ImagesRepository $images
	 * @return Response
	 */
	public function prd(ManagerRegistry $doctrine,Request $request, CategoryRepository $lesCateg, ProduitRepository $lesProds,ImagesRepository $images): Response
	{
		$prod = new Produit();
		$form = $this->createForm(ProduitType::class, $prod);

		$categ = new Category();
		$formC = $this->createForm(CategPrdType::class, $categ);

		$imgs = new Images();
		$formI = $this->createForm(ImagesType::class);
		$entityManager = $doctrine->getManager();


		$formC->handleRequest($request);
		$form->handleRequest($request);
		$formI->handleRequest($request);

		if ($formC->isSubmitted() && $formC->isValid()) {
			$categ = $formC->getData();
			$entityManager->persist($categ);
			$entityManager->flush();
			return $this->redirectToRoute('backoffice_prd');
		}

		if ($form->isSubmitted() && $form->isValid()) {

			$images = $form->get('images')->getData();

			// On boucle sur les images
			foreach($images as $image){
				// On génère un nouveau nom de fichier
				$fichier = md5(uniqid()).'.'.$image->guessExtension();

				// On copie le fichier dans le dossier uploads
				$image->move(
					$this->getParameter('images_directory'),
					$fichier
				);

				// On crée l'image dans la base de données
				$img = new Images();
				$img->setNom($fichier);
				$prod->setNomPrd($form->get('nomPrd')->getData());
				$prod->setSousTitre($form->get('sousTitre')->getData());
				$prod->setCategory($form->get('category')->getData());
				$prod->setPrix($form->get('prix')->getData());
				$prod->setQtt($form->get('qtt')->getData());
				$prod->setDescription($form->get('description')->getData());
				$prod->addImage($img);
			}

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($img);
			$entityManager->persist($prod);
			$entityManager->flush();
			return $this->redirectToRoute('backoffice_prd');
		}

		if ($formI->isSubmitted() && $formI->isValid()) {

			$images = $formI->get('images')->getData();
			$prdLiers = $lesProds->find($formI->get('Produit')->getData()->getId());
			// On boucle sur les images
			foreach($images as $image){
				// On génère un nouveau nom de fichier
				$fichier = md5(uniqid()).'.'.$image->guessExtension();

				// On copie le fichier dans le dossier uploads
				$image->move(
					$this->getParameter('images_directory'),
					$fichier
				);

				// On crée l'image dans la base de données
				$imgs->setNom($fichier);
				$prdLiers->addImage($imgs);
			}

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($imgs);
			$entityManager->persist($prdLiers);
			$entityManager->flush();
			return $this->redirectToRoute("backoffice_prd");

		}

		return $this->render('backoffice/GestionPrd.html.twig', [
			'controller_name' => 'BackofficeController',
			'form' => $form->createView(),
			'formC' => $formC->createView(),
			'formI' => $formI->createView(),
			'lesCateg' => $lesCateg->findAll(),
			'lesProds' => $lesProds->findAll(),
			'images' => $images->findAll(),
		]);
	}

	/**
	 *  Permet la suppression de catégorie
	 *
	 * @Route("/admin-del-categorie-{id}", name="backoffice_delCateg")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 */
	public function delCateg($id,ManagerRegistry $doctrine,Request $request, CategoryRepository $lesCateg): Response
	{
		$entityManager = $doctrine->getManager();
		$entityManager->remove($lesCateg->find($id));
		$entityManager->flush();
		return $this->redirectToRoute('backoffice_prd');
	}

	/**
	 *  Permet la suppression d'image lier à un produit (Car une image ou plusieurs images peuvent appartenir à un produit)
	 *
	 * @Route("/admin-del-image-{id}", name="backoffice_delImg")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 */
	public function delImg($id,ManagerRegistry $doctrine,Request $request, ImagesRepository $lesImg): Response
	{
		$entityManager = $doctrine->getManager();
		$img = $lesImg->find($id);
		$pathToFile = $this->getParameter('images_directory').'/'.$img->getNom();
		if (file_exists($pathToFile)) {
			unlink($pathToFile);
		}
		$entityManager->remove($lesImg->find($id));
		$entityManager->flush();
		return $this->redirectToRoute('backoffice_prd');
	}

	/**
	 *  Permet la suppression de produit
	 *
	 * @Route("/admin-del-produit-{id}", name="backoffice_delProd")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 */
	public function delProd($id,ManagerRegistry $doctrine,Request $request, ProduitRepository $lesPrds,ImagesRepository $lesImg): Response
	{
		$entityManager = $doctrine->getManager();
		$entityManager->remove($lesPrds->find($id));
		foreach ($lesPrds->find($id)->getImages() as $image) {
			$img = $lesImg->find($image->getId());
			$pathToFile = $this->getParameter('images_directory').'/'.$img->getNom();
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
			$entityManager->remove($img);
			$entityManager->flush();
		}
		$entityManager->flush();
		return $this->redirectToRoute('backoffice_prd');
	}



	/**
	 *  Permet la gestion des ventes réaliser sur le site
	 *
	 * @Route("/admin-ventes", name="backoffice_ventes")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 * @return Response
	 */
	public function ventes(): Response
	{
		return $this->render('backoffice/GestionVentes.html.twig', [
			'controller_name' => 'BackofficeController',
		]);
	}


	/**
	 *  Permet l'ajout d'article sur le site.
	 *
	 * @Route("/admin-articles", name="backoffice_article")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 * @param ArticleRepository $art
	 * @param ManagerRegistry $doctrine
	 * @param Request $request
	 * @return Response
	 */
	public function articles(ArticleRepository $art,ManagerRegistry $doctrine,Request $request): Response
	{
		$article = new Article();
		$form = $this->createForm(ArticleType::class, $article);

		$imgs = new Images();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$images = $form->get('image')->getData();
			// On boucle sur les images

			foreach ($images as $image) {


				// On génère un nouveau nom de fichier
				$fichier = md5(uniqid()).'.'.$image->guessExtension();

				// On copie le fichier dans le dossier uploads
				$image->move(
					$this->getParameter('images_directory'),
					$fichier
				);
			}

				// On crée l'image dans la base de données
				$imgs->setNom($fichier);
				$article->setImage($imgs);

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($imgs);
			$entityManager->persist($article);
			$entityManager->flush();

			return $this->redirectToRoute('backoffice_article');
		}


		return $this->render('backoffice/GestionArticles.html.twig', [
			'controller_name' => 'BackofficeController',
			"form"=>$form->createView(),
			"Articles"=>$art->findAll()
		]);
	}


	/**
	 *  Permet la suppression d'article
	 *
	 * @Route("/admin-del-article-{id}", name="backoffice_delArt")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 */
	public function delArt($id,ManagerRegistry $doctrine,Request $request, ArticleRepository $art): Response
	{
		$entityManager = $doctrine->getManager();
		$entityManager->remove($art->find($id));

			$image=$art->find($id);
			$article = $art->find($image->getId());
			$pathToFile = $this->getParameter('images_directory').'/'.$article->getImage();
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
			$entityManager->remove($article);
			$entityManager->flush();

		$entityManager->flush();
		return $this->redirectToRoute('backoffice_article');
	}


	/**
	 * Permet la gestion des devis client
	 * TODO faire la partie de gestion de devis client
	 * @Route("/admin-Devis", name="backoffice_devis")
	 * @IsGranted("ROLE_CHEF", statusCode=404, message="La page n'existe pas.")
	 */
	public function Devis(DevisRepository $devis): Response
	{
		return $this->render('backoffice/GestionDevis.html.twig', [
			'controller_name' => 'BackofficeController',
			"devis"=>$devis->findAll(),
		]);
	}


}
