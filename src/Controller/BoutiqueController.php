<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Facture;
use App\Entity\LigneCommande;
use App\Entity\Produit;
use App\Repository\CategoryRepository;
use App\Repository\FactureRepository;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\Persistence\ManagerRegistry;

class BoutiqueController extends AbstractController
{
	/**
	 * @var RequestStack
	 */
	private $requestStack;

	/**
	 * @param RequestStack $requestStack
	 */
	public function __construct(RequestStack $requestStack)
	{
		$this->requestStack = $requestStack;
	}


	/**
	 * Permet d'afficher tous les produits qui on était ajouté au site
	 *
	 * @param ProduitRepository $prod
	 * @param CategoryRepository $category
	 * @return Response
	 * @Route("/boutique", name="boutique")
	 */
    public function index(ProduitRepository $prod, CategoryRepository $category): Response
    {
		$produit= $prod->findAll();
		if(isset($_POST['categ']) or isset($_POST['tri'])){
			$categ =htmlspecialchars($_POST['categ']);
			$tri = htmlspecialchars($_POST['tri']);
			if($categ != ""){
				$produits = $prod->findByTri_OR_AND_Categ($categ, $tri);
			} else if ($tri != ""){
				$produits = $prod->findByTri_OR_AND_Categ($categ, $tri);
			}
			else if ($tri != "" and $categ != ""){
				$produits = $prod->findByTri_OR_AND_Categ($categ, $tri);
			}
		} else {
			$produits = $produit;
		}
        return $this->render('boutique/index.html.twig', [
            'controller_name' => 'BoutiqueController',
	        "lesProds" => $produits,
	        "category" => $category->findAll(),
        ]);
    }


	/**
	 * Permet le focus sur un produit en particulier (Paramètre spécial : id = prod.id, qtt = la quantité de produit commander [1..5])
	 *
	 * @param ProduitRepository $prod
	 * @param integer $id
	 * @param integer $qtt
	 * @return Response
	 * @Route("/boutique/produit-{id}-{qtt}", name="prod")
	 */
	public function prod(ProduitRepository $prod,$id,$qtt): Response
	{
		$produit= $prod->find($id);
		return $this->render('boutique/prod.html.twig', [
			'controller_name' => 'BoutiqueController',
			"prod" => $produit,
			"qtt"=>$qtt
		]);
	}


	/**
	 * Permet l'ajout au panier d'un produit et d'une quantité (fameux qtt passé dans l'url/paramètre)
	 * Produit sera stocker en session
	 * @Route("/boutique/panier-{id}-{qtt}", name="prodToPanier")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 * @param ProduitRepository $prod
	 * @param integer $id
	 * @param integer $qtt
	 * @return Response
	 */
	public function prodToPanier(ProduitRepository $prod,$id,$qtt): Response
	{
		$session= $this->requestStack->getSession();
		$panier = $session->get("PanierClient");
		if(!isset($panier)){
			$session->set("PanierClient",[]);
		}

		if(!isset($panier)){
			$count=0;
		} else{
			$count=count($panier);
		}

		$panier[$count]["id"]=$id;
		$panier[$count]["qtt"]=$qtt;

		$compt=["id"=>0,"nb"=>0];
		foreach ($panier as $prodPan) {
			if($prodPan["id"]==$id){
				$compt["id"]=$prodPan["id"];
				$compt["nb"]+=$prodPan["qtt"];
			}
		}

		if ($compt["nb"] <= $prod->find($id)->getQtt() and $compt['id']==$id){
			$session->set('PanierClient', $panier);
			return $this->redirectToRoute('panier');
		}else{
			$this->addFlash('err', "Vous ne pouvez commander plus de produits qu'il n'y en a !");
		}

		return $this->redirectToRoute('prod', ['id'=>$id,"qtt"=>$qtt]);

	}

	/**
	 * Permet la suppression d'un produit dans le panier dans la session
	 * @param integer $id
	 * @Route("/boutique/del-{id}", name="delToPanier")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 */
	public function delToPanier($id): Response
	{
		$session= $this->requestStack->getSession();
		$panier = $session->get("PanierClient");


		$panier = array_values($panier);
		unset($panier[$id]);
		$panier = array_values($panier);
		$session->set('PanierClient', $panier);

		return $this->redirectToRoute('panier');
	}

	/**
	 * Permet l'affichage du panier
	 * @Route("/panier", name="panier")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 * @param ProduitRepository $prod
	 * @return Response
	 */
	public function panier(ProduitRepository $prod): Response
	{
		$session= $this->requestStack->getSession();
		$panier= $session->get('PanierClient');
		$produit=[];

		$subTab=[];
		if(!empty($panier)){
			foreach ($panier as $prd) {
				$p=$prod->find($prd['id']);
				$subTab['prd']=$p;
				$subTab['qtt']=$prd['qtt'];
				$subTab["prixhtTT"]=$prd['qtt']*$p->getPrix();
				$produit[]=$subTab;
			}
		}


		return $this->render('boutique/panier.html.twig', [
			'controller_name' => 'BoutiqueController',
			"panier"=>$produit,
		]);
	}


	/**
	 * Permet l'intégration de stripe afin de réaliser des paiements par carte bancaire
	 *
	 * @param ProduitRepository $prod
	 * @param float $fee
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 * @Route("/create-checkout-session/{fee}", name="achat")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 */
	public function achat(ProduitRepository $prod,$fee)
	{
		$user = $this->get('security.token_storage')->getToken()->getUser();
		$session= $this->requestStack->getSession();
		$panier= $session->get('PanierClient');
		$YOUR_DOMAIN = $this->getParameter('app.DOMAIN');
		$produit=[];
		$items=[];

		$subTab=[];
		if(!empty($panier)){
			foreach ($panier as $prd) {
				$p=$prod->find($prd['id']);
				$subTab['prd']=$p;
				$subTab['qtt']=$prd['qtt'];
				$subTab["prixhtTT"]=$prd['qtt']*$p->getPrix();
				$produit[]=$subTab;
			}
		}

		foreach ($produit as $item) {
			$items[]=[
				'price_data' => [
					'currency' => 'eur',
					'product_data' => [
						'name' =>$item['prd']->getNomPrd(),
						'images' => [
							$YOUR_DOMAIN."/images/".$item['prd']->getImages()[0]->getNom()],
					],
					'unit_amount' => round($item['prd']->getPrix()*100),
				],
				'quantity' => $item['qtt'],
			];
		}


		\Stripe\Stripe::setApiKey($this->getParameter('app.STRIPE'));

		$sessionStripe = \Stripe\Checkout\Session::create([
			'customer_email' => $user->getEmail(),
			'submit_type' => 'pay',
			'shipping_address_collection' => [
				'allowed_countries' => ['FR'],
			],
			'shipping_options' => [
				[
					'shipping_rate_data' => [
						'type' => 'fixed_amount',
						'fixed_amount' => [
							'amount' =>  round($fee*100),
							'currency' => 'eur',
						],
						'display_name' => 'Livrer sous ',
						// Delivers between 5-7 business days
						'delivery_estimate' => [
							'minimum' => [
								'unit' => 'business_day',
								'value' => 2,
							],
							'maximum' => [
								'unit' => 'business_day',
								'value' => 10,
							],
						]
					]
				],
			],
			'payment_method_types' => ['card'],
			'line_items' => $items,
			'mode' => 'payment',
			'locale'=>"fr",
			'billing_address_collection'=> 'required',
			'success_url' => $this->generateUrl('success',['fee'=>$fee],UrlGeneratorInterface::ABSOLUTE_URL),
			'cancel_url' => $this->generateUrl('failed',[],UrlGeneratorInterface::ABSOLUTE_URL),
		]);

		//dd($session);
		return $this->json([ 'id' => $sessionStripe->id ]);

	}


	/**
	 * Permet de confirmer l'achat client et décompter le/les produits achetés
	 * @Route("/boutique/achat-succes-{fee}", name="success")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 * @param ManagerRegistry $doctrine
	 * @param ProduitRepository $prod
	 * @param float $fee
	 * @return Response
	 */
	public function sucesse(ManagerRegistry $doctrine,ProduitRepository $prod,$fee): Response
	{
		$user = $this->get('security.token_storage')->getToken()->getUser();
		$newFacture = new Facture();
		$entityManager = $doctrine->getManager();

		$session= $this->requestStack->getSession();
		$panier= $session->get("PanierClient");

		$produit=[];

		$subTab=[];
		if(!empty($panier)){
			foreach ($panier as $prd) {
				$p=$prod->find($prd['id']);
				$subTab['prd']=$p;
				$subTab['qtt']=$prd['qtt'];
				$subTab["prixhtTT"]=$prd['qtt']*$p->getPrix();
				$subTab["fee"]=$fee;
				$produit[]=$subTab;
			}
		}

		$prixTT=0;

		foreach ($produit as $pro) {
			$newLigneCommande = new LigneCommande();
			$prixTT+=$pro['prixhtTT'];
			$newLigneCommande->setCodeClient($user);
			$newLigneCommande->setQtt($pro['qtt']);
			$newLigneCommande->setProduit($pro['prd']);
			$newFacture->addListeProd($newLigneCommande);
			$entityManager->persist($newLigneCommande);
			$entityManager->flush();
		}

		$newFacture->setPrixLivraison($fee);
		$newFacture->setTotalPrixPrd($prixTT);

		$entityManager->persist($newFacture);
		$entityManager->flush();

		$session->remove("PanierClient");

		return $this->redirectToRoute('app_profil',[]);
	}

	/**
	 * Permet de rediriger le client si erreur de paiement.
	 * @Route("/boutique/achat-failed", name="failed")
	 * @IsGranted("ROLE_CLIENT", statusCode=404, message="Connecte-toi pour accéder au panier .")
	 * @return Response
	 */
	public function failed(): Response
	{
		return $this->render('boutique/failed.html.twig', [
			'controller_name' => 'BoutiqueController',
		]);
	}
}
