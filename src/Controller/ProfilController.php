<?php

namespace App\Controller;

use App\Form\PerduType;
use App\Form\ResetType;
use App\Notification\MailNotification;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilController extends AbstractController
{
	/**
	 *
	 * TODO faire le profil client
	 *
	 * @Route("/profil", name="app_profil")
	 */
	public function index(): Response
	{
		return $this->render('profil/index.html.twig', [
			'controller_name' => 'ProfilController',
		]);
	}

	/**
	 * Permet la récupération de mot de passe de façon sécuriser
	 *
	 * @Route("/perdu", name="app_perdu")
	 * @param Request $request
	 * @param UserRepository $user
	 * @param ManagerRegistry $doctrine
	 * @param MailNotification $mailer
	 * @return Response
	 */
	public function perdu(Request $request, UserRepository $user,ManagerRegistry $doctrine,MailNotification $mailer): Response
	{
		$form = $this->createForm(PerduType::class);
		$form->handleRequest($request);
		$entityManager = $doctrine->getManager();

		if ($form->isSubmitted() && $form->isValid()) {
			$mail = $form->getData()['mail'];
			$data = $user->findByMail($mail);
			$code = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
			if(!empty($data)){
				$users = $user->find($data[0]);
				$users->setReset($code);
				$entityManager->persist($users);
				$entityManager->flush();
				$mailer->notifyContact('code de vérification', $mail, $code);

				return $this->redirectToRoute('app_resetMdp',['mail'=>$mail]);
			}

		}
		return $this->render('profil/perdu.html.twig', [
			'controller_name' => 'ProfilController',
			"formU"=>$form->createView(),
		]);
	}

	/**
	 * Permet la vérification du code du client envoyer par mail + modification du mot de passe
	 *
	 * @Route("/reset/{mail}", name="app_resetMdp")
	 * @param Request $request
	 * @param UserRepository $user
	 * @param Request
	 * @param ManagerRegistry $doctrine
	 * @param UserPasswordEncoderInterface $encoder
	 * @return Response
	 */
	public function reset(Request $request, UserRepository $user,$mail,ManagerRegistry $doctrine,UserPasswordEncoderInterface $encoder): Response
	{
		$entityManager = $doctrine->getManager();
		$form = $this->createForm(ResetType::class);
		$form->handleRequest($request);
		$err ="";
		if ($form->isSubmitted() && $form->isValid()) {
			$data = $user->findByMail($mail);
			$code = $form->getData()['code'];
			if(!empty($data)){
				$users =$user->findByMail($mail)[0]->getReset();
				if($users == $code){
					$users = $user->find($data[0]);
					$encoded = $encoder->encodePassword($users, $form->getData()['mdp']);
					$users->setPassword($encoded);
					$users->setReset("OULALA");
					$entityManager->persist($users);
					$entityManager->flush();
					return $this->redirectToRoute('app_login');
				} else{
					$err="code invalide !";
				}
			}
		}

		return $this->render('profil/reset.html.twig', [
			'controller_name' => 'ProfilController',
			"formU"=>$form->createView(),
			"err"=>$err
		]);
	}

}
