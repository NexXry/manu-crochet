<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ContactType;

class IndexController extends AbstractController
{
	/**
	 * Permet d'afficher la page d'accueil
	 *
	 * @Route("/", name="index")
	 * @param ProduitRepository $prd
	 * @param ArticleRepository $art
	 * @return Response
	 */
    public function index(ProduitRepository $prd,ArticleRepository $art): Response
    {

        $form = $this->createForm(ContactType::class);

        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'form' => $form->createView(),
	        "produits"=>$prd->findSortAcceuil(),
	        "articles"=>$art->findSortAcceuil()
        ]);
    }

    /**
     * Affiche la page de mention légale / CGU
     * @Route("/mention-légale", name="mention")
     * @return Response
     */
    public function mention(): Response
    {

        return $this->render('index/mention.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

	/**
	 * Affiche la page avec tous les articles
	 * @Route("/articles", name="articles")
	 * @param ArticleRepository $article
	 * @return Response
	 */
	public function articles( ArticleRepository $article): Response
	{

		return $this->render('index/articles.html.twig', [
			'controller_name' => 'IndexController',
			"articles"=>$article->findAll()
		]);
	}

	/**
	 * Permet de faire le focus sur un article et de pouvoir le consulter en détail
	 * @Route("/article-{id}", name="article")
	 * @param ArticleRepository $article
	 * @param integer $id
	 * @return Response
	 */
	public function article($id, ArticleRepository $article): Response
	{

		return $this->render('index/article.html.twig', [
			'controller_name' => 'IndexController',
			"article"=>$article->find($id)
		]);
	}



}
